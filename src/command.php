<?php
namespace coldApe;

trait command
{
    protected $error = '';

    protected $mime_list = [];

    protected $extension_list = [];

    protected $max_size = '';

    protected $file_name = '';

    protected $file_size = '';

    protected $file_mime = '';

    protected $extension = '';

    protected $path = '';

    protected $public_path = '/';

    protected $tmp_path = '';

    public $file = [];

    /**
     * 检测文件是否存在 如果需要覆盖相同文件名则改为false
     *
     * @var bool
     */
    public $check_file_exists = true;


/*-----------------------------------设置----------------------------------*/

    /**
     * 统一设置类属性
     *
     * @param $file
     * @return bool
     * @throws uploadException
     */
    protected function set($file)
    {
        // 上传用的
        if (is_array($file)) {
            $this->tmp_path = $file['tmp_name'];

            $this->file_mime = $file['type'];

            $this->file_size = $file['size'];

            $name = explode('.',  $file['name']);

            $this->extension = array_pop($name);

            $this->file_name = join('', $name);

            return true;
        }

        if (!file_exists($file)) {
            $this->setError('文件不存在');
        }

        // 普通文件用的
        $this->tmp_path = $file;

        $this->file_mime = mime_content_type($file);

        $this->file_size = filesize($file);

        $path = explode('.',  $file);

        $this->extension = array_pop($path);

        $name = explode('/', join('', $path));

        $this->file_name = array_pop($name);

        $this->setPath(join('/', $name));
    }

    /**
     * 设置文件名
     *
     * @param string $filename 文件名
     * @return $this
     */
    public function setName($filename)
    {
        $this->file_name = $filename;

        return $this;
    }

    /**
     * 设置文件名后缀
     *
     * @param string $suffix 前缀名
     * @return $this
     */
    public function setSuffix($suffix)
    {
        $this->file_name .= $suffix;

        return $this;
    }

    /**
     * 设置文件名前缀
     *
     * @param string $prefix 前缀名
     * @return $this
     */
    public function setPrefix($prefix)
    {
        $this->file_name = $prefix.$this->file_name;

        return $this;
    }

    /**
     * 设置路径
     *
     * @param string $path 文件路径
     * @return $this
     */
    public function setPath($path)
    {
        $this->path = $path;

        $this->public_path = '/'.$path;

        return $this;
    }

    /**
     * 设置限制的MIME列表
     *
     * @param string|array $mime MIME列表[可以是字符串或者关联数组]
     * @return $this
     */
    public function setMimeLimit($mime)
    {
        if (!is_array($mime)) {
            $mime = [$mime];
        }

        $this->mime_list = $mime;

        return $this;
    }

    /**
     * 设置文件输出扩展名 用于转换格式等
     *
     * @param $extension
     * @return $this
     */
    public function setExtension($extension)
    {
        $this->extension = $extension;

        return $this;
    }

    /**
     * 设置限制的扩展列表
     *
     * @param string|array $extension 扩展名列表[可以是字符串或者关联数组]
     * @return $this
     */
    public function setExtensionLimit($extension)
    {
        if (!is_array($extension)) {
            $extension = [$extension];
        }

        // 统一转为小写
        $extension = array_map(function($val){ return strtolower($val); }, $extension);

        $this->extension_list = $extension;

        return $this;
    }

    /**
     * 设置访问路径
     *
     * @param string $path 外部访问路径
     * @return $this
     */
    public function setPublicPath($path)
    {
        $this->public_path = ($path[-1] !== '/') ? $path.'/' : $path;

        $this->public_path = ($this->public_path[0] !== '/') ? '/'.$this->public_path : $this->public_path;

        return $this;
    }

    /**
     * 设置文件限制大小
     *
     * @param string $size 文件大小(支持单位:B,K,M,G)
     * @return $this
     */
    public function setMaxSize($size)
    {
        $size_number = intval($size);

        $size_type = str_replace($size_number, '', $size);

        switch ($size_type) {
            case 'B':
                $this->max_size = $size_number;
                break;
            case 'K':
                $this->max_size = $size_number * 1024;
                break;
            case 'M':
                $this->max_size = $size_number * 1048576;
                break;
            case 'G':
                $this->max_size = $size_number * 1073741824;
                break;
        }

        return $this;
    }

    /**
     * 设置错误信息
     *
     * @param string $error 错误内容
     * @throws uploadException
     */
    protected function setError($error)
    {
        $this->error = $error;

        throw new uploadException($error);
    }

/*-----------------------------------设置----------------------------------*/


/*-----------------------------------获取----------------------------------*/

    /**
     * 获取文件的MIME值
     *
     * @return string
     */
    public function getMime()
    {
        return $this->file_mime;
    }

    /**
     * 获取文件名
     *
     * @return string
     */
    public function getName()
    {
        return $this->file_name;
    }

    /**
     * 获取带有后缀的文件名
     *
     * @return string
     */
    public function getFullName()
    {
        return $this->getName() . '.' . $this->extension;
    }

    /**
     * 获取文件路径
     *
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * 获取包含文件名的完整路径
     *
     * @return string
     */
    public function getFullPath()
    {
        return $this->path . '/' . $this->getFullName();
    }

    /**
     * 获取文件外部访问链接
     *
     * @return string
     */
    public function getPublicPath()
    {
        $http = ($_SERVER['SERVER_PORT'] == '80') ? 'http://' : 'https://';

        return $http.$_SERVER['SERVER_NAME'].$this->public_path.$this->getFullName();
    }

    /**
     * 获取扩展名
     *
     * @return string
     */
    public function getExtension()
    {
        return $this->extension;
    }

    /**
     * 获取文件大小
     *
     * @return string
     */
    public function getSize()
    {
        return $this->file_size;
    }

    /**
     * 获取文件的MD5值
     */
    public function getMd5()
    {
        return md5_file($this->getFullPath());
    }

    /**
     * 获取文件宽高和视频音频长度
     *
     * @return array|bool
     */
    public function getFileInfo()
    {
        // 获取文件宽高
        $size = getimagesize($this->tmp_path);

        if ($size) {
            $return = [
                'width' => $size[0],
                'height' => $size[1]
            ];
        } else {
            $return = $this->getFfmpegInfo($this->tmp_path);

            if (empty($return)) {
                return false;
            }
        }

        return $return;
    }

    /**
     * 获取视频长度和宽高
     * @param string $video_path
     * @return array
     */
    private function getFfmpegInfo($video_path)
    {
        $shell = "ffmpeg -i {$video_path} 2>&1";

        exec($shell, $info, $status);

        // 设置默认值
        $return = [
            'length' => 1,
            'width' => 0,
            'height' => 0,
        ];

        // 设置默认值
        if (is_array($info)) {
            // 通过返回的数组，去匹配 Duration
            foreach ($info as $v) {
                // 查找时长
                if (strpos($v, 'Duration') !== false) {
                    $length = substr($v, stripos($v, '.') - 8, 8);

                    $length = explode(":", $length);

                    $length[2] = empty($length[2]) ? 2 : intval($length[2]);

                    $return['length'] = $length[2];
                }

                // 正则匹配宽高
                if (strpos($v, 'Stream') !== false) {
                    preg_match('/(\d){2,9}x(\d)+/', $v, $video_match);

                    if (empty($video_match)) {
                        continue;
                    }

                    $width_height_arr = explode("x", $video_match[0]);

                    $return['width'] = $width_height_arr[0] ?? 0;

                    $return['height'] = $width_height_arr[1] ?? 0;
                }
            }
        }

        return $return;
    }


/*-----------------------------------获取----------------------------------*/

/*-----------------------------------其他----------------------------------*/

    /**
     * 检测文件大小
     *
     * @return $this|void
     * @throws uploadException
     */
    protected function checkSize()
    {
        // 没有设置最大值则为不限
        if (empty($this->max_size)) {
            return $this;
        }

        // 文件大小 大于 限制大小 则报错
        if ($this->file_size <= 0 || $this->file_size > $this->max_size) {
            $this->setError('检测文件大小：文件超过限制大小');
        }

        return $this;
    }

    /**
     * 检测扩展名
     *
     * @throws uploadException
     */
    protected function checkExtension()
    {
        // 没有设置限制扩展名则为不限
        if (empty($this->extension_list)) {
            return $this;
        }

        // 文件扩展名不在限制扩展名列表内则报错
        if (!in_array(strtolower($this->extension), $this->extension_list)) {
            $this->setError('检测文件扩展名：文件扩展名不在限制范围内');
        }

        return $this;
    }

    /**
     * 检测MIME
     *
     * @throws uploadException
     */
    protected function checkMime()
    {
        // 没有设置限制扩展名则为不限
        if (empty($this->mime_list)) {
            return $this;
        }

        // 文件扩展名不在限制扩展名列表内则报错
        if (!in_array($this->file_mime, $this->mime_list)) {
            $this->setError('检测MIME：文件MIME不在限制范围内');
        }

        return $this;
    }

    /**
     * 检查文件是否存在
     *
     * @return bool 存在则返回true 否则则返回false
     * @throws uploadException
     */
    protected function checkExists()
    {
        $file = $this->getFullPath();

        if (!$this->check_file_exists) {
            return true;
        }

        // 路径为空
        if (empty($file)) {
            $this->setError('检查文件是否存在：文件路径为空');
        }

        if (file_exists($file)) {
            $this->setError('检查文件是否存在：文件名已存在，请重新设置文件名');
        }

        return file_exists($file);
    }

    /**
     * 检查文件上传路径是否存在 不存在则创建
     *
     * @return $this
     * @throws uploadException
     */
    protected function checkPath()
    {
        $path = $this->path;

        // 路径为空
        if (empty($path)) {
            $this->setError('检查文件路径：请先设置文件上传路径');
        }

        // 检查文件夹路径是否存在
        if (is_dir($path)) {
            return $this;
        }

        // 不存在则创建
        if (!@mkdir($path, 0755, true)) {
            // 防止并发出现创建失败而进入
            sleep(0.5);

            // 清除判断文件夹是否存在的缓存
            clearstatcache();

            if (!is_dir($path)) {
                $this->setError('检查文件路径：路径创建失败');
            }
        }

        return $this;
    }

    /**
     * 生成唯一文件名
     *
     * @return string
     */
    public function uniqueName()
    {
        $str = '';

        $rand_str = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';

        $rand_str .= strtolower($rand_str);

        $length = strlen($rand_str);

        for ($i = 0; $i < 10; $i++) {
            $str .= $rand_str[rand(0, $length - 1)];
        }

        return uniqid($str);
    }

/*-----------------------------------其他----------------------------------*/

}